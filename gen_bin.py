#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2020 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
import shutil
import argparse
from utils import exec_command
from utils import makedirs
import platform
import tarfile



def main():
    parser = argparse.ArgumentParser(description='Generate binary')
    parser.add_argument('--path', help='Build output path')
    parser.add_argument('--board', help='Board type')
    parser.add_argument('--kernel', help='OHOS kernel type')
    parser.add_argument('--compile_so', help='So strip command')
    parser.add_argument('--compile_bin', help='Bin strip command')
    args = parser.parse_args()

    if args.board != 'ivy5661':
        return

    if args.path:
        output_path = os.path.abspath(args.path)

        inputfile = output_path+"/bin/"+args.board
        outfile = output_path+"/bin/"+args.board+".bin"
        print("betsy:::")
        print(inputfile)
    else:
        return -1
    mkbin_path = os.path.abspath(args.compile_bin)
    print(mkbin_path)
    mkbin=mkbin_path+"/arm-none-eabi-objcopy"


    cmd = [mkbin, "-O", "binary","-S",  inputfile,outfile ]
    print (cmd)


    log=os.path.join(output_path, 'build.log')
    exec_command(cmd, log_path=log)
  
    return 0


if __name__ == "__main__":
    sys.exit(main())
